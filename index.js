  /////////////////////
 //     Classes     //
/////////////////////
/*
	To create a class in jS, we use the class keyword and {}.
	Naming convention for classes: Begins with UpperCase Characters.
*/

class Dog {
	// We add a constructor method to a class to be able to initialize values upon instatiation of an object from a class.
	// "Instattioation" -  technical term when creating an object from a class.
	// An object created from a class is called an instance.
	constructor(name,breed,age){
		this.name = name;
		this.breed = breed;
		this.age = age * 7;
	}
}
// Instantiate a new object from the class
// "new" keyword is used to instantiate an object from a class
let dog1 = new Dog("Bantay","Chihuahua",3);
// console.log(dog1);

class Person {
	constructor(name,age,nationality,address) {
		this.name = name;
		// this.age = age;
		this.nationality = nationality;
		this.address = address;
		
		if(typeof age !== "number"){
			this.age = undefined;
		} else {
			this.age = age;
		}
	}
	greet() {
		console.log(`Hello! Good Morning!`);
		// This must be returned if the intended method can be chained.
		return this;
	};
	introduce() {
		// We should be able to return this, so that when used in a chain, we can still have an access to the reference of this.
		console.log(`Hi! My name is ${this.name}`);
		return this;
	};
	chagedAddress(newAddress) {
		// We can also update the field of our object by referring to the property via "this" keyword and reassigning a new value.
		this.address = newAddress;
		return this;
	}
}

// let person1 = new Person("Juan Dela Cruz",30,"Filipino","Cebu City Ph.");
// let person2 = new Person("Juana Dela Cruz",27,"Filipino","Davao City Ph.");

// console.log(person1);
// console.log(person2);

// Chain methods from an instance
// person1.greet().introduce();
// person2.greet().introduce();

class Student {
	constructor(name,email,grades) {
		this.name = name;
		this.email = email;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		if(grades.some(eachGrade => typeof eachGrade !== 'number')){
			this.grades = undefined
		} else{
			this.grades = grades;
		}
	}
	login() {
		console.log(`${this.email} has logged in`);
		return this;
	};
	logout() {
		console.log(`${this.email} has logged out`);
		return this;
	};
	listGrades() {
		this.grades.forEach(grade => {
		    console.log(grade);
		})
		return this;
	};
	computeAve() {
		let sum = this.grades.reduce((accumulator,num)=>{
			return accumulator+=num;
		})
		this.average = sum/this.grades.length;
		return this;
	};
	willPass() {
		 if(Math.round(this.average) >= 85){
		 	this.isPassed = true;
		 } else {
		 	this.isPassed = false;
		 }
		 return this;
	};
	willPassWithHonor() {
		if(this.isPassed && Math.round(this.average) >= 90) {
			this.isPassedWithHonors = true;	
		} else if(this.willPass()){
			this.isPassedWithHonors = false;
		} else {
			this.isPassedWithHonors = undefined;
		}
		return this;
	};
}

let student1 = new Student('John','john@mail.com',[89, 84, 78, 88]);
let student2 = new Student('Joe','joe@mail.com',[78, 82, 79, 85]);
let student3 = new Student('Jane','jane@mail.com',[87, 89, 91, 83]);
let student4 = new Student('Jessie','jessie@mail.com',[91, 89, 92, 93]);

console.log(student1);
console.log(student2);
console.log(student3);
console.log(student4);

student1.login().logout().listGrades().computeAve().willPass().willPassWithHonor();
student2.login().logout().listGrades().computeAve().willPass().willPassWithHonor();
student3.login().logout().listGrades().computeAve().willPass().willPassWithHonor();
student4.login().logout().listGrades().computeAve().willPass().willPassWithHonor();